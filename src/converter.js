/**
 * Padding outputs 2 characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}
const dap = (hex) => {
    return parseInt(hex,16)
}

module.exports = {
    rgbToHex: (red,green,blue) => {
        const redHex = red.toString(16); // 0-255 -> 0-ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        return pad(redHex) + pad(greenHex) + pad(blueHex);
    },
    hexToRgb: (hex)=>{
        const redHex = hex.substring(0,2) // 05 (1478) -> 05 -> 5
        const greenHex = hex.substring(2,4) // (05) 14 (78) -> 14 -> 20
        const blueHex = hex.substring(4,6) // (0514) 78 -> 78 -> 120

        return [dap(redHex), dap(greenHex), dap(blueHex)]
    }
}
