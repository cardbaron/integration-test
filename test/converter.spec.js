const expect = require("chai").expect
const converter = require("../src/converter")

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => {
        it("should convert RGB to Hex", () => {
            const redHex = converter.rgbToHex(255,0,0) // ff0000
            const greenHex = converter.rgbToHex(0,255,0) // 00ff00
            const blueHex = converter.rgbToHex(0,0,255) // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })

    describe("Hex to RGB conversion", ()=>{
        it("should convert Hex to RGB", ()=>{
            const Hex = "051478"
            const RGB = converter.hexToRgb(Hex)

            expect(RGB[0]).to.equal(5)
            expect(RGB[1]).to.equal(20)
            expect(RGB[2]).to.equal(120)
        })
    })
})
